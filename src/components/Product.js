import React, { useState } from 'react';

import { Grid, Typography, Fab, Tooltip, makeStyles } from '@material-ui/core';
import AddIcon from '@material-ui/icons/Add';
import RemoveIcon from '@material-ui/icons/Remove';

const useStyles = makeStyles((theme) => ({
	fab: {
		margin: theme.spacing(2),
	},
}));

function Product(props) {
	const { id, name, price, addCountHandler, removeCountHandler } = props;

	const [count, setCount] = useState(0);

	const classes = useStyles();

	const handleAddCount = () => {
		setCount((count) => count + 1);
		addCountHandler(id);
	};

	const handleSubtractCount = () => {
		setCount((count) => count - 1);
		removeCountHandler(id);
	};

	return (
		<Grid item xs={12}>
			<Grid container alignItems='center' spacing={2}>
				<Grid item>
					<Typography variant='h6'>{name} - </Typography>
				</Grid>
				<Grid item>
					<Typography variant='h6'>${price}</Typography>
				</Grid>
				<Grid item>
					<Tooltip title='Add' aria-label='add'>
						<Fab color='primary' className={classes.fab}>
							<AddIcon onClick={handleAddCount} />
						</Fab>
					</Tooltip>
				</Grid>
				<Grid>
					<Grid item>
						<Typography variant='h6'>{count}</Typography>
					</Grid>
				</Grid>
				<Grid item>
					<Tooltip title='Remove' aria-label='remove'>
						<Fab
							disabled={count === 0}
							onClick={handleSubtractCount}
							color='primary'
							className={classes.fab}
						>
							<RemoveIcon />
						</Fab>
					</Tooltip>
				</Grid>
			</Grid>
		</Grid>
	);
}

export default Product;
