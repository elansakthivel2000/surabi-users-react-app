import React from 'react';

import { Grid, List, ListItem, ListItemText } from '@material-ui/core';

function Order(props) {
	const { productName, productCount } = props;
	return (
		<Grid item>
			<List component='nav' aria-label='secondary mailbox folders'>
				<ListItem button>
					<ListItemText primary={productName} />
				</ListItem>

				<ListItem button>
					<ListItemText primary={productCount} />
				</ListItem>
			</List>
		</Grid>
	);
}
export default Order;
