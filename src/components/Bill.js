import React from 'react';

import { Grid, Typography, Paper, makeStyles } from '@material-ui/core';
import Order from './Order';

const useStyles = makeStyles((theme) => ({
	root: {
		flexGrow: 1,
	},
	paper: {
		padding: theme.spacing(2),
		margin: 'auto',
		marginTop: '100px',
		maxWidth: 500,
	},
	textField: {
		padding: '10px',
	},
}));

function Bill(props) {
	const { id, totalAmount, numItems, orders } = props;
	const classes = useStyles();

	return (
		<Paper className={classes.paper}>
			<Grid item xs={12}>
				<Grid container alignItems='center' spacing={3}>
					<Grid item>
						<Typography variant='h6'>Bill Number: {id}</Typography>
					</Grid>
					<Grid item>
						<Typography variant='h6'>Bill Amount: ${totalAmount}</Typography>
					</Grid>
					<Grid item>
						<Typography variant='h6'>Number of Items: {numItems}</Typography>
					</Grid>
				</Grid>
				<Grid container direction='column'>
					{orders.map((order) => (
						<Order
							productName={order.product.productName}
							productCount={order.productCount}
							key={order.id}
						/>
					))}
				</Grid>
			</Grid>
		</Paper>
	);
}

export default Bill;
