import { useCallback, useEffect, useState } from 'react';
import { Route, Switch, Redirect, useHistory } from 'react-router-dom';

import Auth from './containers/Auth';
import MyOrders from './containers/MyOrders';
import Navigation from './containers/Navigation';
import Products from './containers/Products';

import { AuthContext } from './context/AuthContext';

function App() {
	const [userId, setUserId] = useState(null);
	const [username, setUsername] = useState(null);

	const history = useHistory();

	const login = useCallback((userId, username) => {
		setUserId(userId);
		setUsername(username);
		localStorage.setItem('userId', userId);
		localStorage.setItem('username', username);
	}, []);

	const logout = () => {
		setUserId(null);
		setUsername(null);
		localStorage.removeItem('userId');
	};

	useEffect(() => {
		const userId = localStorage.getItem('userId');
		const username = localStorage.getItem('username');
		if (userId) {
			login(userId, username);
		}
	}, [login]);

	let routes;

	if (userId) {
		routes = (
			<Switch>
				<Route exact path='/products'>
					<Products />
				</Route>
				<Route exact path='/myOrders'>
					<MyOrders />
				</Route>
				<Redirect to='/products' />
			</Switch>
		);
	} else {
		routes = (
			<Switch>
				<Route exact path='/auth'>
					<Auth />
				</Route>
				<Redirect
					to={{
						pathname: '/auth',
						state: { from: history.location.pathname },
					}}
				/>
			</Switch>
		);
	}
	return (
		<AuthContext.Provider
			value={{
				userId: userId,
				login: login,
				logout: logout,
				username: username,
			}}
		>
			<Navigation />
			<main>{routes}</main>
		</AuthContext.Provider>
	);
}

export default App;
