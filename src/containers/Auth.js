import React, { useContext, useState } from 'react';
import {
	CircularProgress,
	makeStyles,
	Paper,
	Grid,
	TextField,
	Button,
	Snackbar,
} from '@material-ui/core';
import MuiAlert from '@material-ui/lab/Alert';

import axios from 'axios';
import { useHistory } from 'react-router';
import { AuthContext } from '../context/AuthContext';

const useStyles = makeStyles((theme) => ({
	root: {
		flexGrow: 1,
	},
	paper: {
		padding: theme.spacing(2),
		margin: 'auto',
		marginTop: '100px',
		maxWidth: 500,
	},
	textField: {
		padding: '10px',
	},
}));

function Auth(props) {
	const classes = useStyles();

	const auth = useContext(AuthContext);

	const history = useHistory();
	console.log(history.location.state);

	const [isLoginMode, setIsLoginMode] = useState(true);
	const [username, setUsername] = useState('');
	const [password, setPassword] = useState('');
	const [isLoading, setIsLoading] = useState(false);
	const [error, setError] = useState(null);
	const [snackBarOpen, setSnackBarOpen] = useState(false);

	const handleSwitchLoginMode = () => {
		setIsLoginMode((prevState) => !prevState);
	};

	const handleSnackBarClose = () => {
		setSnackBarOpen(false);
	};

	const handleChange = (event) => {
		if (event.target.name === 'username') {
			setUsername(event.target.value);
		} else {
			setPassword(event.target.value);
		}
	};

	const handleAuth = async () => {
		let url;
		if (isLoginMode) {
			url = '/api/users/login';
		} else {
			url = '/api/users/register';
		}
		try {
			setIsLoading(true);
			const response = await axios.post(`http://localhost:8080/${url}`, {
				username,
				password,
			});
			const user = response.data;
			if (user.role === 'ADMIN') {
				console.log('My code was here...');
				setIsLoading(false);
				setUsername('');
				setPassword('');
				setError("You're an admin. Please use the admin app.");
				setSnackBarOpen(true);
				return;
			}

			setIsLoading(false);
			auth.login(user.id, user.username);
		} catch (error) {
			const errorMessage = error?.response?.data?.message
				? error.response.data.message
				: 'Something went wrong..';

			setIsLoading(false);
			setError(errorMessage);
			setSnackBarOpen(true);
		}
	};

	return (
		<div className={classes.root}>
			<Paper className={classes.paper}>
				{isLoading ? (
					<CircularProgress />
				) : (
					<Grid
						className={classes.textField}
						container
						direction='column'
						justifyContent='center'
						alignItems='center'
					>
						<Grid className={classes.textField} item xs={12}>
							<h1>{isLoginMode ? 'Login' : 'Sign up'}</h1>
						</Grid>
						<Grid className={classes.textField} item xs={12}>
							<TextField
								required
								id='username'
								label='Username'
								variant='outlined'
								name='username'
								onChange={handleChange}
								value={username}
							/>
						</Grid>
						<Grid className={classes.textField} item xs={12}>
							<TextField
								required
								id='password'
								label='Password'
								variant='outlined'
								name='password'
								type='password'
								onChange={handleChange}
								value={password}
							/>
						</Grid>
						<Grid className={classes.textField} item xs={12}>
							<Button variant='contained' color='primary' onClick={handleAuth}>
								{isLoginMode ? 'Login' : 'Sign up'}
							</Button>
						</Grid>

						<Grid className={classes.textField} item xs={12}>
							<h3>
								{isLoginMode ? 'New User ? ' : 'Already have an account ? '}
								<Button color='secondary' onClick={handleSwitchLoginMode}>
									{isLoginMode ? 'Signup' : 'Login'}
								</Button>
							</h3>
						</Grid>
						{snackBarOpen && (
							<Snackbar
								anchorOrigin={{
									vertical: 'top',
									horizontal: 'center',
								}}
								open={snackBarOpen}
								autoHideDuration={6000}
								onClose={handleSnackBarClose}
							>
								<MuiAlert
									elevation={6}
									variant='filled'
									onClose={handleSnackBarClose}
									severity='error'
								>
									{error}
								</MuiAlert>
							</Snackbar>
						)}
					</Grid>
				)}
			</Paper>
		</div>
	);
}

export default Auth;
