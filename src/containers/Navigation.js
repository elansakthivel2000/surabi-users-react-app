import React, { useContext } from 'react';
import axios from 'axios';

import { useHistory } from 'react-router-dom';

import { AuthContext } from '../context/AuthContext';

import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';

const useStyles = makeStyles((theme) => ({
	root: {
		flexGrow: 1,
	},
	menuButton: {
		marginRight: theme.spacing(2),
	},
	title: {
		flexGrow: 1,
	},
}));

function Navigation(props) {
	const classes = useStyles();

	const history = useHistory();

	const auth = useContext(AuthContext);

	const handleLogout = async () => {
		await axios.post(
			'http://localhost:8080/api/users/logout',
			{},
			{
				headers: {
					Authorization: auth.userId,
				},
			}
		);
		auth.logout();
	};

	return (
		<AppBar position='static'>
			<Toolbar>
				<Typography variant='h6' className={classes.title}>
					Surabi - Users
				</Typography>
				{auth.userId && (
					<Button color='inherit' onClick={() => history.push('/products')}>
						Products
					</Button>
				)}
				{auth.userId && (
					<Button color='inherit' onClick={() => history.push('/myOrders')}>
						My Orders
					</Button>
				)}
				{auth.userId && (
					<Button color='inherit' onClick={handleLogout}>
						Logout
					</Button>
				)}
				{auth.userId && (
					<Typography style={{ marginLeft: '10px' }} variant='h6'>
						{auth.username}
					</Typography>
				)}
			</Toolbar>
		</AppBar>
	);
}

export default Navigation;
