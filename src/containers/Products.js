import React, { useContext, useEffect, useState } from 'react';
import clsx from 'clsx';

import {
	Grid,
	Typography,
	Button,
	makeStyles,
	CircularProgress,
	Snackbar,
} from '@material-ui/core';

import MuiAlert from '@material-ui/lab/Alert';
import { green } from '@material-ui/core/colors';

import axios from 'axios';
import Product from '../components/Product';
import { AuthContext } from '../context/AuthContext';
import { useHistory } from 'react-router';

const useStyles = makeStyles((theme) => ({
	wrapper: {
		margin: theme.spacing(3),
		position: 'relative',
	},
	buttonSuccess: {
		backgroundColor: green[500],
		'&:hover': {
			backgroundColor: green[700],
		},
	},
	fabProgress: {
		color: green[500],
		position: 'absolute',
		top: -6,
		left: -6,
		zIndex: 1,
	},
	buttonProgress: {
		color: green[500],
		position: 'absolute',
		top: '50%',
		left: '50%',
		marginTop: -12,
		marginLeft: -12,
	},
}));

function Products(props) {
	const classes = useStyles();
	const auth = useContext(AuthContext);
	const history = useHistory();

	const [products, setProducts] = useState([]);
	const [cart, setCart] = useState({});

	const [totalPrice, setTotalPrice] = useState(0);
	const [loading, setLoading] = React.useState(false);
	const [success, setSuccess] = React.useState(false);
	const [error, setError] = useState(null);
	const [snackBarOpen, setSnackBarOpen] = useState(false);

	useEffect(() => {
		const fetchData = async () => {
			const response = await axios.get('http://localhost:8080/api/products');
			setProducts(response.data);
		};

		fetchData();
	}, []);

	const buttonClassname = clsx({
		[classes.buttonSuccess]: success,
	});

	const findPrice = (id) => {
		return products.find((product) => product.id === id).price;
	};

	const addProductCountToCart = (id) => {
		const newCart = {
			...cart,
		};
		newCart[id] = newCart[id] ? newCart[id] + 1 : 1;
		setCart(newCart);
		const price = findPrice(id);
		setTotalPrice((prevPrice) => {
			let newPrice = prevPrice + price;
			newPrice = Math.round(newPrice * 100) / 100;
			return newPrice;
		});
	};

	const removeProductCountFromCart = (id) => {
		const newCart = {
			...cart,
		};
		newCart[id] = newCart[id] ? newCart[id] - 1 : 1;
		setCart(newCart);
		const price = findPrice(id);
		setTotalPrice((prevPrice) => {
			let newPrice = prevPrice - price;
			newPrice = Math.round(newPrice * 100) / 100;
			return newPrice;
		});
	};

	const handlePlaceOrder = async () => {
		if (!loading) {
			setSuccess(false);
			setLoading(true);
			try {
				await axios.post(
					'http://localhost:8080/api/bills',
					{ cart: cart },
					{
						headers: {
							Authorization: auth.userId,
						},
					}
				);
				setSuccess(true);
				setLoading(false);
				history.push('/myOrders');
			} catch (error) {
				const errorMessage = error?.response?.data?.message
					? error.response.data.message
					: 'Something went wrong..';

				setError(errorMessage);
				setSnackBarOpen(true);

				setSuccess(false);
				setLoading(false);
			}
		}
	};

	const handleSnackBarClose = () => {
		setSnackBarOpen(false);
	};

	return (
		<Grid container direction='column' justifyContent='center' alignItems='center'>
			{products.map((product) => (
				<Product
					key={product.id}
					id={product.id}
					name={product.productName}
					price={product.price}
					addCountHandler={addProductCountToCart}
					removeCountHandler={removeProductCountFromCart}
				/>
			))}
			<Grid item>
				<Typography variant='h4' color='primary' style={{ marginTop: '30px' }}>
					Total Price = ${totalPrice}
				</Typography>
			</Grid>
			<Grid className={classes.wrapper}>
				<Button
					variant='contained'
					color='primary'
					className={buttonClassname}
					disabled={loading || totalPrice <= 0}
					onClick={handlePlaceOrder}
				>
					Place Order
				</Button>
				{loading && <CircularProgress size={24} className={classes.buttonProgress} />}
			</Grid>
			{snackBarOpen && (
				<Snackbar
					anchorOrigin={{
						vertical: 'top',
						horizontal: 'center',
					}}
					open={snackBarOpen}
					autoHideDuration={6000}
					onClose={handleSnackBarClose}
				>
					<MuiAlert
						elevation={6}
						variant='filled'
						onClose={handleSnackBarClose}
						severity='error'
					>
						{error}
					</MuiAlert>
				</Snackbar>
			)}
		</Grid>
	);
}

export default Products;
