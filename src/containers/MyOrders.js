import { Grid } from '@material-ui/core';
import axios from 'axios';
import { useContext, useEffect, useState } from 'react';
import Bill from '../components/Bill';
import { AuthContext } from '../context/AuthContext';

function MyOrders(props) {
	const [purchases, setPurchases] = useState([]);

	const auth = useContext(AuthContext);

	useEffect(() => {
		const fetchData = async () => {
			try {
				const response = await axios.get(
					`http://localhost:8080/api/users/${auth.userId}`
				);
				console.log(response.data);
				setPurchases(response.data.purchases);
			} catch (error) {
				console.log(error.response);
			}
		};

		fetchData();
	}, [auth.userId]);

	return (
		<Grid
			container
			direction='column'
			justifyContent='center'
			alignItems='center'
			spacing={1}
		>
			{purchases.map((purchase) => (
				<Bill
					key={purchase.id}
					id={purchase.id}
					totalAmount={purchase.billAmount}
					numItems={purchase.numItems}
					orders={purchase.orders}
				/>
			))}
		</Grid>
	);
}

export default MyOrders;
