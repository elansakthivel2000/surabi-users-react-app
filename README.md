# Surabi - Users

## Steps To Run

1. [Surabi API Repository](https://gitlab.com/elansakthivel2000/surabi-api) Refer this repository's README file on how to run the server. (if you already have this cloned in your system, pull the master branch to get all the latest commits).
2. After starting the server, clone this react app.
3. Run npm install inside the root directory.
4. Run npm start
5. Please make sure that the server is running the entire time while you're testing the react app.
